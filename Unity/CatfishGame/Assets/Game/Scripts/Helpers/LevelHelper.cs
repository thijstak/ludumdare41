﻿using System.Collections;
using Assets.Game.Scripts.DataObjects;
using UnityEngine;

namespace Assets.Game.Scripts.Helpers
{
	public static class LevelHelper
	{
		public static void LevelUp(int newLevel, EntityAttributes att)
		{
			int oldLevel = (int)GlobalManager.Instance.BaseAttribute.GetLevel(att);
			int lvlUp = newLevel - oldLevel;
			if (lvlUp > 0)
			{
				lvlUp *= GlobalManager.Instance.Growth.StatsPerLevel;
				for (int i = 0; i < lvlUp; i++)
				{
					int value = Random.Range(0, 3);
					switch (value)
					{
						case 0:
							att.Strength++;
							break;
						case 1:
							att.Constitution++;
							break;
						case 2:
							att.Agility++;
							break;
					}
				}
			}
		}
	}
}
