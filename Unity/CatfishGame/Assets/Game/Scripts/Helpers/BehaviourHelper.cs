﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor.WindowsStandalone;
using UnityEngine;

namespace Assets.Game.Scripts.Helpers
{
	public static class BehaviourHelper
	{
		public static bool IsInRange(Entity entity, Vector3 target, float range)
		{
			float distance = Vector3.Distance(target, entity.transform.position);
			return distance <= range;
		}

		public static void MoveTo(Entity entity, Vector3 target)
		{
			Vector3 newVelocity = (target - entity.transform.position).normalized * entity.Attributes.Agility;
			entity.Body.velocity = newVelocity;
			entity.transform.rotation = Quaternion.LookRotation(newVelocity.normalized);
		}
	}
}
