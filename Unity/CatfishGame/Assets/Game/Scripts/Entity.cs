﻿using System.Collections.Generic;
using Assets.Game.Scripts.DataObjects;
using Assets.Game.Scripts.Tasks;
using UnityEngine;

public class Entity : MonoBehaviour
{
	// Unity values.
	public Rigidbody Body;

	// Custom values.
	public EntityAttributes Attributes = new EntityAttributes();
	public Queue<BehaviourBase> Behaviours = new Queue<BehaviourBase>();
	public float Level;
	public Health Health = new Health();
	public bool IsAlive = true;

	// Use this for initialization
	protected void Start()
	{
		Body = GetComponent<Rigidbody>();
		UpdateStats();
		Health.Heal(Health.MaxHealth);
		IsAlive = true;
	}

	protected void Update()
	{
		if (!IsAlive) return;

		// Kill me if I die?
		IsAlive = Health.IsAlive();
		if (!IsAlive)
		{
			gameObject.SendMessage("OnDeath");
		}
	}

	// Update is called once per frame
	protected void FixedUpdate()
	{
		if (!IsAlive) return;

		if (Behaviours.Count > 0)
		{
			var result = Behaviours.Peek().Update(this);
			if (result != BehaviourBase.BehaviourResult.InProgress)
			{
				Behaviours.Dequeue();
			}
		}
	}

	public void UpdateStats()
	{
		Health.CalculateMaxHealth(Attributes);
		Level = GlobalManager.Instance.BaseAttribute.GetLevel(Attributes);
		float scaleValue = GlobalManager.Instance.Growth.ScalePerLevel * Level;
		gameObject.transform.localScale = new Vector3(scaleValue, scaleValue, scaleValue);
	}
}
