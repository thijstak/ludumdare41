﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RTSCamera : MonoBehaviour
{
	public float CameraVerticalMovementSpeed = 10.0f;

	private Camera camera;

	public SpeedValues Speeds = new SpeedValues();
	private float yaw = 0.0f;
	private float pitch = 0.0f;

	// Use this for initialization
	void Start()
	{
		camera = GetComponent<Camera>();

		// Set the default camera direction.
		var r = transform.eulerAngles;
		pitch = r.x;
		yaw = r.y;
	}

	// Update is called once per frame
	void Update()
	{
		var f = new Vector3(transform.forward.x, 0, transform.forward.z);
		var r = new Vector3(transform.right.x, 0, transform.right.z);

		var x = Input.GetAxis("Horizontal") * r * Time.deltaTime * Speeds.ForwardSpeed;
		var z = Input.GetAxis("Vertical") * f * Time.deltaTime * Speeds.SideSpeed;
		var y = Input.GetAxis("Zoom") * CameraVerticalMovementSpeed * Time.deltaTime * Speeds.UpSpeed;

		// transform.Translate(x, y, z);
		transform.position += x + z;
		transform.position += new Vector3(0, y, 0);

		var grab = Input.GetButton("CameraGrab");
		var mx = Input.GetAxis("Mouse X");
		var my = Input.GetAxis("Mouse Y");

		if (grab)
		{
			yaw += Speeds.MouseRotationHorizontal * mx * Time.deltaTime;
			pitch -= Speeds.MouseRotationVertical * my * Time.deltaTime;

			transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);
		}

		if (transform.position.x > Speeds.MovementDistance)
		{
			transform.position = new Vector3(Speeds.MovementDistance, transform.position.y, transform.position.z);
		}

		if (transform.position.x < -Speeds.MovementDistance)
		{
			transform.position = new Vector3(-Speeds.MovementDistance, transform.position.y, transform.position.z);
		}

		if (transform.position.y > Speeds.MaxHeight)
		{
			transform.position = new Vector3(transform.position.x, Speeds.MaxHeight, transform.position.z);
		}

		if (transform.position.y < Speeds.Floor)
		{
			transform.position = new Vector3(transform.position.x, Speeds.Floor, transform.position.z);
		}

		if (transform.position.z > Speeds.MovementDistance)
		{
			transform.position = new Vector3(transform.position.x, transform.position.y, Speeds.MovementDistance);
		}

		if (transform.position.z < -Speeds.MovementDistance)
		{
			transform.position = new Vector3(transform.position.x, transform.position.y, -Speeds.MovementDistance);
		}
	}

	[Serializable]
	public class SpeedValues
	{
		public float ForwardSpeed = 10.0f;
		public float SideSpeed = 10.0f;
		public float UpSpeed = 10.0f;
		public float MouseRotationHorizontal = 10.0f;
		public float MouseRotationVertical = 10.0f;
		public float MovementDistance = 125f;
		public float MaxHeight = 100.0f;
		public float Floor = 15.0f;
	}
}
