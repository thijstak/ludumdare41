﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using Assets.Game.Scripts.DataObjects;
using Assets.Game.Scripts.Helpers;
using UnityEngine;

public class FishSpawner : MonoBehaviour
{
	public FishSpawnerData SpawnData = new FishSpawnerData();
	public Entity[] SpawnPrefab;

	public float currentTime;
	private int entityCounter = 0;

	// Use this for initialization
	void Start()
	{
		ResetTimer();
		SpawnData.AddMissing(); // The will resolve all data that is 0 with that of the global manager except the total spawn amount.
	}

	// Update is called once per frame
	void Update()
	{
		currentTime -= Time.deltaTime;
		if (currentTime <= 0)
		{
			Spawn();
			ResetTimer();
		}
	}

	/// <summary>
	/// Trigger if one of our fish has perished.
	/// </summary>
	public void RegisterDeath()
	{
		entityCounter--;
	}

	/// <summary>
	/// Spawn fish.
	/// </summary>
	void Spawn()
	{
		if (SpawnData.LimitTotalSpawns > 0 && entityCounter >= SpawnData.LimitTotalSpawns) return;

		if (SpawnPrefab != null && entityCounter < SpawnData.MaximumNumberOfActiveSpawns)
		{
			// Get the amount of fish we should spawn.
			int amount = Mathf.Min(SpawnData.MaximumNumberOfActiveSpawns - entityCounter,
				Random.Range(SpawnData.MinimumNumberOfFishPerSpawn, SpawnData.MaximumNumberOfFishPerSpawn + 1));

			for (int i = 0; i < amount; i++)
			{
				// Create the fish.
				var o = Object.Instantiate(SpawnPrefab[Random.Range(0, SpawnPrefab.Length)], transform.position,
					transform.rotation);

				// Set the owner if possible.
				var behaviour = o.GetComponent<SpawnedBehaviour>();
				if (behaviour != null)
				{
					behaviour.Owner = this;
				}

				LevelEntity(o);

				o.gameObject.SetActive(true);

				// Increment our counter.
				entityCounter++;
			}
		}
	}

	void LevelEntity(Entity entity)
	{
		int newLevel = Random.Range(SpawnData.MinimumLevelOfSpawns, SpawnData.MaximumLevelOfSpawns + 1);
		LevelHelper.LevelUp(newLevel, entity.Attributes);
	}

	/// <summary>
	/// Reset spawn timer.
	/// </summary>
	void ResetTimer()
	{
		currentTime = Random.Range(SpawnData.MinimumTimeBetweenSpawnsInSeconds, SpawnData.MaximumTimeBetweenSpawnsInSeconds);
	}
}