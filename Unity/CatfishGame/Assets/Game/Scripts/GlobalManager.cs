﻿using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Game.Scripts.DataObjects;
using UnityEngine;

public class GlobalManager : MonoBehaviour
{
	public static GlobalManager Instance;

	public CatfishConstants Catfish = new CatfishConstants();
	public BehaviourConstants Behaviour = new BehaviourConstants();
	public FishSpawnerData FishSpawner = new FishSpawnerData();
	public BasicPrayBehaviourData BasicPreBehaviour = new BasicPrayBehaviourData();
	public BaseAttributeData BaseAttribute = new BaseAttributeData();
	public GrowthData Growth = new GrowthData();
	public CombatData Combat = new CombatData();

	void Awake()
	{
		Instance = this;
	}

	[Serializable]
	public class CatfishConstants
	{
	}

	[Serializable]
	public class BehaviourConstants
	{
		/// <summary>
		/// The minimum distance needed form the target to get a target reached event.
		/// </summary>
		public float MinimumTargetJumpDistance = 0.5f;
		public float FloatUpSpeed = 3.0f;
		public float SinkSpeed = 3.0f;
		public float DespawnHeight = 150f;
	}
}


