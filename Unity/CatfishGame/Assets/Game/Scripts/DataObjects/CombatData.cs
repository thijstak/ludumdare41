﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Game.Scripts.DataObjects
{
	[Serializable]
	public class CombatData
	{
		public float AttackTime = 1.0f;
		public float AttackSpeedBoost = 2.5f;
		public float RecoveryTime = 2.5f;

		public float DamagePerStrengthMeele = 5.0f;
		public float DamagePerStrengthGlave = 7.0f;
		public float DamagePerStrengthHarpoon = 3.0f;
	}
}
