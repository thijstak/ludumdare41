﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Game.Scripts.DataObjects
{
	public class Equipment
	{
		public enum EquipmentType
		{
			None,
			Glave,
			Harpoon
		}

		public EquipmentType Type;
		public float Damage;
		public float Range;
		public float Ammo;
		public int Rank;

		public float GetDamage(Entity entity)
		{
			float damage = Damage;
			switch (Type)
			{
				case EquipmentType.None:
					damage += entity.Attributes.Strength * GlobalManager.Instance.Combat.DamagePerStrengthMeele;
					break;
				case EquipmentType.Glave:
					damage += entity.Attributes.Strength * GlobalManager.Instance.Combat.DamagePerStrengthGlave;
					break;
				case EquipmentType.Harpoon:
					damage += entity.Attributes.Strength * GlobalManager.Instance.Combat.DamagePerStrengthHarpoon;
					break;
			}

			return damage;
		}
	}

	public class Inventory
	{
		private GameObject CarriedObject;

		public void Grab(GameObject o)
		{
			CarriedObject = o;
			// We need to notify the object it has been released.
		}

		public void Release(GameObject o)
		{
			if (CarriedObject != null)
			{
				// We need to notify the object it has been released.
			}

			CarriedObject = null;
		}
	}
}
