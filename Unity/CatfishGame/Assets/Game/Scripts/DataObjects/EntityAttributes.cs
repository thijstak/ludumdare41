﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Game.Scripts.DataObjects
{
	[Serializable]
	public class EntityAttributes
	{
		public float Strength = 1;
		public float Constitution = 1;
		public float Agility = 1;
	}

	[Serializable]
	public class BaseAttributeData
	{
		public float HealthPerStrenth = 6;
		public float HealthPerConstitution = 15;
		public float HealthPerAgility = 3;

		public float GetMaxHealth(EntityAttributes attributes)
		{
			return (attributes.Strength * HealthPerStrenth) +
				   (attributes.Constitution * HealthPerConstitution) +
				   (attributes.Agility * HealthPerAgility);
		}

		public float GetLevel(EntityAttributes att)
		{
			float multiplier = 1.0f/GlobalManager.Instance.Growth.StatsPerLevel;
			return att.Strength * multiplier
				   + att.Constitution * multiplier
				   + att.Agility * multiplier;
		}
	}
}
