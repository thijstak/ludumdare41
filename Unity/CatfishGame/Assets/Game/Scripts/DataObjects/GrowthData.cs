﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Game.Scripts.DataObjects
{
	[Serializable]
	public class GrowthData
	{
		public float ScalePerLevel = 0.33f;
		public int StatsPerLevel = 3;
	}
}
