﻿using System;
using UnityEngine;

namespace Assets.Game.Scripts.DataObjects
{
	[Serializable]
	public class Health
	{
		public float CurrentHealth;
		public float MaxHealth;

		public void CalculateMaxHealth(EntityAttributes att)
		{
			MaxHealth = GlobalManager.Instance.BaseAttribute.GetMaxHealth(att);
		}

		public void Heal(float amount)
		{
			CurrentHealth = Mathf.Min(CurrentHealth + amount, MaxHealth);
		}

		public void TakeDamage(float amount)
		{
			CurrentHealth -= amount;
		}

		public bool IsAlive()
		{
			return CurrentHealth > 0;
		}
	}
}
