﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Game.Scripts.DataObjects
{
	[Serializable]
	public class FishSpawnerData
	{
		public int MinimumNumberOfFishPerSpawn = 0;
		public int MaximumNumberOfFishPerSpawn = 0;
		public float MinimumTimeBetweenSpawnsInSeconds = 0;
		public float MaximumTimeBetweenSpawnsInSeconds = 0;
		public int MaximumNumberOfActiveSpawns = 0;
		public int MinimumLevelOfSpawns = 0;
		public int MaximumLevelOfSpawns = 0;
		public int LimitTotalSpawns = 0;

		public void AddMissing()
		{
			if (MinimumNumberOfFishPerSpawn == 0)
			{
				MinimumNumberOfFishPerSpawn = GlobalManager.Instance.FishSpawner.MinimumNumberOfFishPerSpawn;
			}

			if (MaximumNumberOfFishPerSpawn == 0)
			{
				MaximumNumberOfFishPerSpawn = GlobalManager.Instance.FishSpawner.MaximumNumberOfFishPerSpawn;
			}

			if (MinimumTimeBetweenSpawnsInSeconds == 0)
			{
				MinimumTimeBetweenSpawnsInSeconds = GlobalManager.Instance.FishSpawner.MinimumTimeBetweenSpawnsInSeconds;
			}

			if (MaximumTimeBetweenSpawnsInSeconds == 0)
			{
				MaximumTimeBetweenSpawnsInSeconds = GlobalManager.Instance.FishSpawner.MaximumTimeBetweenSpawnsInSeconds;
			}

			if (MaximumNumberOfActiveSpawns == 0)
			{
				MaximumNumberOfActiveSpawns = GlobalManager.Instance.FishSpawner.MaximumNumberOfActiveSpawns;
			}

			if (MinimumLevelOfSpawns == 0)
			{
				MinimumLevelOfSpawns = GlobalManager.Instance.FishSpawner.MinimumLevelOfSpawns;
			}

			if (MaximumLevelOfSpawns == 0)
			{
				MaximumLevelOfSpawns = GlobalManager.Instance.FishSpawner.MaximumLevelOfSpawns;
			}
		}
	}
}
