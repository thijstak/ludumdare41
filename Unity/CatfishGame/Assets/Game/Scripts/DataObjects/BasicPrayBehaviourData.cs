﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Game.Scripts.DataObjects
{
	[Serializable]
	public class BasicPrayBehaviourData
	{
		public float MinimumIdleTimeBetweenMovement = 0;
		public float MaximumIdleTimeBetweenMovement = 0;
		public float MaximumDistanceFromSpawner = 0;
	}
}
