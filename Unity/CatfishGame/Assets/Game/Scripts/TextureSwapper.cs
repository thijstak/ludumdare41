﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureSwapper : MonoBehaviour
{
	public Texture[] Textures;

	// Use this for initialization
	void Start()
	{
		GetComponent<Renderer>().material.mainTexture = Textures[Random.Range(0, Textures.Length)];
	}

	// Update is called once per frame
	void Update()
	{

	}
}
