﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Game.Scripts.Tasks
{
	class GrabObject : BehaviourBase
	{
		public GrabObject(GameObject target)
		{
			this.target = target;
		}

		private GameObject target;

		public override BehaviourResult Update(Entity entity)
		{
			var cat = entity as CatfishBehaviour;
			if (cat == null)
			{
				return BehaviourResult.Aborted;
			}

			cat.Inventory.Grab(target);
			target.transform.parent = cat.InventorySlot.transform;
			target.transform.position = Vector3.zero;
			var rb = target.GetComponent<Rigidbody>();
			if (rb != null)
			{
				rb.velocity = Vector3.zero;
			}
			return BehaviourResult.Completed;
		}
	}
}
