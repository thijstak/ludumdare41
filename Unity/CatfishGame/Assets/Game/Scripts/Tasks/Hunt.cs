﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Game.Scripts.Helpers;
using UnityEngine;
using UnityEngine.Collections;

namespace Assets.Game.Scripts.Tasks
{
	public class Hunt : BehaviourBase
	{
		public Hunt(Entity target)
		{
			this.target = target;
			currentState = HuntState.MovingInRange;
		}

		enum HuntState
		{
			MovingInRange,
			Attacking,
			Recovering
		}

		private HuntState currentState = HuntState.MovingInRange;
		private Entity target;
		private float attackTime;
		private float recoveryTime;


		public override BehaviourResult Update(Entity entity)
		{
			if (target == null) return BehaviourResult.Aborted;

			switch (currentState)
			{
				case HuntState.MovingInRange:
					MovingInRange(entity);
					return BehaviourResult.InProgress;
				case HuntState.Attacking:
					Attacking(entity);
					return BehaviourResult.InProgress;
				case HuntState.Recovering:
					if (target.IsAlive)
					{
						Recovering(entity);
						return BehaviourResult.InProgress;
					}
					else
					{
						return BehaviourResult.Completed;
					}
			}

			return BehaviourResult.Aborted;
		}

		BehaviourResult MovingInRange(Entity e)
		{
			if (BehaviourHelper.IsInRange(e, target.transform.position, CalculateRange(e)))
			{
				attackTime = GlobalManager.Instance.Combat.AttackTime;
				e.Body.velocity *= GlobalManager.Instance.Combat.AttackSpeedBoost;
				target.Health.TakeDamage(e.Attributes.Strength * GlobalManager.Instance.Combat.DamagePerStrengthMeele);
				currentState = HuntState.Attacking;
				return BehaviourResult.Completed;
			}

			BehaviourHelper.MoveTo(e, target.transform.position);
			return BehaviourResult.InProgress;
		}

		BehaviourResult Attacking(Entity e)
		{
			attackTime -= Time.fixedDeltaTime;
			if (attackTime <= 0)
			{
				currentState = HuntState.Recovering;
				e.Body.velocity = Vector3.zero;
				recoveryTime = GlobalManager.Instance.Combat.RecoveryTime;
				return BehaviourResult.Completed;
			}

			return BehaviourResult.InProgress;
		}

		BehaviourResult Recovering(Entity e)
		{
			recoveryTime -= Time.fixedDeltaTime;
			if (recoveryTime <= 0)
			{
				if (target.Health.IsAlive())
				{
					currentState = HuntState.MovingInRange;
				}

				return BehaviourResult.Completed;
			}

			return BehaviourResult.InProgress;
		}

		float CalculateRange(Entity e)
		{
			return 10f;
		}
	}
}
