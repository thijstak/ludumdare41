﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Assets.Game.Scripts.Helpers;
using UnityEngine;

namespace Assets.Game.Scripts.Tasks
{
	public class MoveToTarget : BehaviourBase
	{
		public MoveToTarget(Vector3 target)
		{
			this.target = target;
		}

		public Vector3 target;

		public override BehaviourResult Update(Entity entity)
		{
			float distance = Vector3.Distance(target, entity.transform.position);

			// Are we there yet?
			if (distance <= GlobalManager.Instance.Behaviour.MinimumTargetJumpDistance * (entity.Attributes.Agility / 10))
			{
				entity.transform.position = target;
				entity.Body.velocity = Vector3.zero;
				return BehaviourResult.Completed;
			}
			else
			{
				BehaviourHelper.MoveTo(entity, target);
				return BehaviourResult.InProgress;
			}
		}
	}
}
