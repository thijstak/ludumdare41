﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Game.Scripts.Tasks
{
	/// <summary>
	/// Our basic do nothing for a while task.
	/// </summary>
	public class Idle : BehaviourBase
	{
		public Idle(float time)
		{
			this.time = time;
		}

		public float time;

		public override BehaviourResult Update(Entity entity)
		{
			time -= Time.deltaTime;
			if (time <= 0)
			{
				return BehaviourResult.Completed;
			}
			return BehaviourResult.InProgress;
		}
	}
}
