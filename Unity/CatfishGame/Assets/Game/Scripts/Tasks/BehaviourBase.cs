﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Game.Scripts.Tasks
{
	public abstract class BehaviourBase
	{
		public enum BehaviourResult
		{
			InProgress,
			Completed,
			Aborted
		}

		public abstract BehaviourResult Update(Entity entity);

		/// <summary>
		/// Abort the current task.
		/// </summary>
		/// <param name="entity">The entity to manipulate.</param>
		public virtual void AbortAction(Entity entity)
		{
			// By default nothing needs to be done.
			// We need this for inventory management tasks.
		}
	}
}
