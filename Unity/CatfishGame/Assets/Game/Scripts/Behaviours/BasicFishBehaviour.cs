﻿using System.Collections;
using System.Collections.Generic;
using Assets.Game.Scripts.FishTypes;
using Assets.Game.Scripts.Tasks;
using UnityEngine;

public class BasicFishBehaviour : MonoBehaviour
{
	private BasicPrey smallFish;

	// Use this for initialization
	void Start()
	{
		smallFish = GetComponent<BasicPrey>();
	}

	// Update is called once per frame
	void Update()
	{
		// Check if our fish needs orders.
		if (smallFish.Behaviours.Count == 0)
		{
			GenerateOrders();
		}
	}

	void GenerateOrders()
	{
		var position = (Random.insideUnitSphere
			* GlobalManager.Instance.BasicPreBehaviour.MaximumDistanceFromSpawner) + smallFish.spawnedBehaviour.transform.position;
		float idleTime = Random.Range(GlobalManager.Instance.BasicPreBehaviour.MinimumIdleTimeBetweenMovement,
			GlobalManager.Instance.BasicPreBehaviour.MaximumDistanceFromSpawner);

		smallFish.Behaviours.Enqueue(new MoveToTarget(position));
		smallFish.Behaviours.Enqueue(new Idle(idleTime));
	}
}
