﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnOnDeath : MonoBehaviour
{
	public GameObject[] PossibleSpawns;
	public int AmountOfSpawns;

	void OnDeath()
	{
		int amount = Random.Range(1, AmountOfSpawns + 1);
		for (int i = 0; i < amount; i++)
		{
			GameObject.Instantiate(PossibleSpawns[Random.Range(0, AmountOfSpawns)]);
		}
	}
}
