﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatUpOnDeath : MonoBehaviour {

	// Update is called once per frame
	void OnDeath()
	{
		var b = gameObject.GetComponent<Rigidbody>();
		if (b != null)
		{
			var dir = Vector3.up * GlobalManager.Instance.Behaviour.FloatUpSpeed;
			b.velocity = dir;
		}
	}
}
