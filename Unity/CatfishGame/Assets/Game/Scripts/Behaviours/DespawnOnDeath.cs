﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DespawnOnDeath : MonoBehaviour
{
	void OnDeath()
	{
		GameObject.Destroy(gameObject);
	}
}
