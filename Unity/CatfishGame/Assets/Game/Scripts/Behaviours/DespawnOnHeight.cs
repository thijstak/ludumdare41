﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DespawnOnHeight : MonoBehaviour
{
	void FixedUpdate()
	{
		if (transform.position.y >= GlobalManager.Instance.Behaviour.DespawnHeight)
		{
			GameObject.Destroy(gameObject);
		}
	}
}
