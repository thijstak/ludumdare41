﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SinkOnDeath : MonoBehaviour
{
	// Update is called once per frame
	void OnDeath()
	{
		var b = gameObject.GetComponent<Rigidbody>();
		if (b != null)
		{
			b.velocity = Vector3.down * GlobalManager.Instance.Behaviour.SinkSpeed;
		}
	}
}
