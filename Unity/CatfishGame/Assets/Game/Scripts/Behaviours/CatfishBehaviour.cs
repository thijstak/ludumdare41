﻿using System.Collections;
using System.Collections.Generic;
using Assets.Game.Scripts.DataObjects;
using Assets.Game.Scripts.Helpers;
using UnityEngine;

public class CatfishBehaviour : Entity
{
	public Inventory Inventory = new Inventory();
	public Equipment Equipment = new Equipment();

	public GameObject Eat;
	public GameObject InventorySlot;
	public GameObject Glave;
	public GameObject Harpoon1;
	public GameObject Harpoon2;
	public GameObject Harpoon3;
	public GameObject Harpoon4;
	public GameObject Harpoon5;
	public GameObject Harpoon6;

	// Use this for initialization
	void Start()
	{
		LevelHelper.LevelUp(25, Attributes);
		base.Start();
	}

	// Update is called once per frame
	void Update()
	{
		base.Update();
	}

	void FixedUpdate()
	{
		base.FixedUpdate();
	}
}
