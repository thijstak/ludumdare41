﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MessageOnDeath : MonoBehaviour
{
	public string Message;

	void OnDeath()
	{
		gameObject.SendMessage(Message);
	}
}
