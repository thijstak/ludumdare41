﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnedBehaviour : MonoBehaviour
{
	public FishSpawner Owner;

	void OnDeath()
	{
		Owner.RegisterDeath();
	}
}
