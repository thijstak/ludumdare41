﻿using System.Collections;
using System.Collections.Generic;
using Assets.Game.Scripts.Tasks;
using UnityEngine;

public class TestScript : MonoBehaviour
{
	private Entity controller;

	// Use this for initialization
	void Start()
	{
		controller = GetComponent<Entity>();
		//controller.Behaviours.Enqueue(new MoveToTarget(new Vector3(100, 100, 55)));
		//controller.Behaviours.Enqueue(new MoveToTarget(new Vector3(-100, 25, -100)));
		//controller.Behaviours.Enqueue(new MoveToTarget(new Vector3(25, 50, 50)));
	}

	private bool isDone = false;
	// Update is called once per frame
	void Update()
	{
		if (isDone)
		{
			return;
		}
		var target = GameObject.FindGameObjectWithTag("Prey");
		if (target == null) return;
		var entity = target.GetComponent<Entity>();
		if (entity == null) return;
		controller.Behaviours.Enqueue(new Hunt(entity));
		controller.Behaviours.Enqueue(new GrabObject(entity.gameObject));
		isDone = true;
		GameObject.Destroy(this);
	}
}
